package t1.dkhrunina.tm.api.repository;

import t1.dkhrunina.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    Boolean isEmailExist(String email);

    Boolean isLoginExist(String login);

    List<User> findAll();

    User findByEmail(String email);

    User findById(String id);

    User findByLogin(String login);

    User remove(User user);

}