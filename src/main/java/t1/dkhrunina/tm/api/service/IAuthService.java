package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.model.User;

public interface IAuthService {

    User register(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

}