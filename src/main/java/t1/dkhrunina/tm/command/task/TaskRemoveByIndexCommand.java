package t1.dkhrunina.tm.command.task;

import t1.dkhrunina.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    private static final String NAME = "t-remove-by-index";

    private static final String DESCRIPTION = "Remove task by index.";

    @Override
    public void execute() {
        System.out.println("[Remove task by index]");
        System.out.println("Enter index: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().removeByIndex(index);
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}