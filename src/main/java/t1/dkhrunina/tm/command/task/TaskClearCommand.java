package t1.dkhrunina.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    private static final String NAME = "t-clear";

    private static final String DESCRIPTION = "Delete all tasks.";

    @Override
    public void execute() {
        System.out.println("[Clear task list]");
        getTaskService().clear();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}