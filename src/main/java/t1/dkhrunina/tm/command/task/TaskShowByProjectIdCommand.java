package t1.dkhrunina.tm.command.task;

import t1.dkhrunina.tm.model.Task;
import t1.dkhrunina.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    private static final String NAME = "t-show-by-project";

    private static final String DESCRIPTION = "Show tasks by project id.";

    @Override
    public void execute() {
        System.out.println("[Task list by project id]");
        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(projectId);
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.toString());
            index++;
        }
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}