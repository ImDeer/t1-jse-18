package t1.dkhrunina.tm.exception.field;

public class RoleEmptyException extends AbstractFieldException {

    public RoleEmptyException() {
        super("Error: role is empty");
    }

}